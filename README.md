node-red-assem-robot-sim
========================
A Node-RED project for lightweight browser-based planning and simulation of assembly sequences on robotic cells.

# Getting started

## Prerequisites
You will need Node-RED - as this is a Node-RED project. You can get started with it [here]([https://nodered.org/docs/getting-started/](https://nodered.org/docs/getting-started/)) should you need. Also, the flows file is encrypted and needs a key to view in the Node-RED editor. Write an e-mail on that to <bazs96@gmail.com>.

## Running with Node-RED projects enabled
With the projects feature enabled, open your graphical flow editor. In its menu, go to **Projects > New** and create a new project by cloning this repository.

And you are good to go. A guide on Node-RED projects can be found [here]([https://nodered.org/docs/user-guide/projects/](https://nodered.org/docs/user-guide/projects/)).

## Running on Windows w/o Node-RED projects
Download the contents of or clone the repository in **C:\\Users\\*your_username*\\.node-red\\projects\\node-red-assem-robot-sim\\** . After that, you can import the flows file in your flows editor and you are good to go.

All this is important, as the *file in* nodes will look for the files to serve for the GUI there. Optionally, if your Node-RED directories are set up some other way, you can always edit the *file in* nodes in the flows once imported.

## Running on Linux w/o Node-RED projects
Download the contents of or clone the repository in **/home/*your_username*/.node-red/projects/node-red-assem-robot-sim/** . After that, you can import the flows file in your flows editor and you are good to go.

All this is important, as the *file in* nodes will look for the files to serve for the GUI there. Optionally, if your Node-RED directories are set up some other way, you can always edit the *file in* nodes in the flows once imported.
