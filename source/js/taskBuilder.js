// ***************************************************************************
// Interface
// ***************************************************************************
const TASKBUILDER = {
    check: function () {
        var flag = true;
        if (typeof PUBSUBFRAMEWORK === "undefined") { console.log("[ ERRO ] task.check(): pubsubFramework is not defined"); flag = false; }
        if (flag) console.log("[ INFO ] taskBuilder.check(): OK");
        return flag;
    }
};
// ***************************************************************************
// Content
// ***************************************************************************
var WAITING_4_INVERSE = false;
var TRAFOS;
var BOX_IDs;

function simpleTask(target,id,cmd,param,desc="",status=0) {
    return {
        "status": status,
        "target": target,
        "id": id,
        "command":{
            "cmd": cmd,
            "param": param
        },
        "desc":""
    };
}
function complexEntry(id,parents=[0],subEntries=[]) {
    return {
        "complex": true,
        "number": id,
        "task": {
            "subEntries": subEntries
        },
        "parents": parents
    };
}
function subEntry(target,cmd,param,status=0) {
    return {
        "data": {
            "target": target,
            "status": status,
            "command": {
                "cmd": cmd,
                "param": param
            }
        }
    };
}
function entry(target,id,cmd,param=[],parents=[0],status=0) {
    return {
        "number": id,
        "task": {
            "target": target,
            "command": {
                "cmd": cmd,
                "param": param
            },
            "status": status
        },
        "parents": parents
    };
}
function taskGraph(graphID,entries=[]) {
    return {
        "graphID": graphID,
        "entries": entries
    };
}

function TCPOffsetDistance(trafo,time,distance,id=listenSession){
    return subEntry(id,"Inverse",[
        [
            trafo[0], trafo[1], trafo[2] + distance,
            trafo[3]*trafo[6], trafo[4]*trafo[6], trafo[5]*trafo[6]
        ],
        time]);
}
function SafetyDistance(trafo,time,id=listenSession){
    return TCPOffsetDistance(trafo,time,300,id);
}
function CloseIn(trafo,time,id=listenSession){
    return TCPOffsetDistance(trafo,time,175,id);
}
function OpenGripper(time,id=listenSession){
    return subEntry(id,"Interpolate",[
        ["gripR1", "gripR3", "gripR5"],
        [354, 77.1791, 229.538],
        time]);
}
function CloseGripper(time,id=listenSession){
    return subEntry(id,"Interpolate",[
        ["gripR1", "gripR3", "gripR5"],
        [369.1828, 61.9845, 244.738],
        time]);
}
function PickTask(id, target=listenSession){
    return subEntry(target,"Pick",id);
}
function PutTask(id,target=listenSession){
    return subEntry(target,"Put",id);
}
function createGraphListElement(graph) {
    var d = new Date();
    var listElem = document.createElement("li");

    //var hyperlink = document.createElement("a");
    //hyperlink.setAttribute("href","data:text/plain," + JSON.stringify(graph));
    //hyperlink.download = graph.graphID.replace(".","_") + "_" + String(d.getHours()) + "h" + String(d.getMinutes()) + "m.json";
    //hyperlink.appendChild(document.createTextNode(graph.graphID));
    //listElem.appendChild(hyperlink);

    listElem.appendChild(document.createTextNode(graph.graphID));
    listElem.setAttribute("onclick",  "document.getElementById('graphDisplay').innerHTML='" + JSON.stringify(graph) + "';"
                                    + "document.getElementById('graphLoadButton').setAttribute('onclick', 'publishMessage(\"Graph@NRC\"," + JSON.stringify(graph) + ");"
                                        + "document.getElementById(\"jsonModal\").style.display=\"none\";');"
                                    + "document.getElementById('jsonModal').style.display='block';");
    return listElem;
}