// ***************************************************************************
// Interface
// ***************************************************************************
const X3DTOOLS = {
    check: function () {
        var flag = true;
        if (flag) console.log("[ INFO ] x3dTools.check(): OK");
        return flag;
    }
};
// ***************************************************************************
// Content
// ***************************************************************************
function delElem(id) {
    var elem = getElem(id);
    elem.parentNode.removeChild(elem);
    return 0;
}
function createTransform(translation,rotation,id = null) {
    var tr = document.createElement('Transform');
    tr.setAttribute("translation", translation.join(' '));
    tr.setAttribute("rotation", rotation.join(' '));
    if(id !== null) tr.setAttribute("id", id);
    return tr;
}
function createBox(size,color,id = null) {
    var tr2 = document.createElement('Transform');
    tr2.setAttribute("translation",[size[0]/2,size[1]/2,size[2]/2].join(' '));
    if(id !== null) tr2.setAttribute("id", id);
    var sh = document.createElement('Shape');
    var ap = document.createElement('Appearance');
    var ma = document.createElement('Material');
    ma.setAttribute("diffusecolor",color.join(' '));
    ap.appendChild(ma);
    sh.appendChild(ap);
    var bx = document.createElement('Box');
    bx.setAttribute("size",size.join(' '));
    sh.appendChild(bx);
    tr2.appendChild(sh);
    return tr2;
}
function createSphere(radius,color,id = null) {
    var tr = document.createElement('Transform');
    if(id !== null) tr.setAttribute("id",id);
    var sh = document.createElement('Shape');
    var ap = document.createElement('Appearance');
    var ma = document.createElement('Material');
    ma.setAttribute("diffusecolor",color.join(' '));
    ap.appendChild(ma);
    sh.appendChild(ap);
    var bx = document.createElement('Sphere');
    bx.setAttribute("radius",radius);
    sh.appendChild(bx);
    tr.appendChild(sh);
    return tr;
}
function getElem(id) {
    return document.getElementById(id);
}
function addElem(elem,world) {
    getElem(world).appendChild(elem);
    return 0;
}