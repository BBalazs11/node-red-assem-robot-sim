// ***************************************************************************
// Interface
// ***************************************************************************
const PICKPUT = {
    check: function () {
        var flag = true;
        if (typeof CHAT === "undefined") { console.log("[ ERRO ] pickPut.check(): chat is not defined"); flag = false; }
        if (typeof X3DTOOLS === "undefined") { console.log("[ ERRO ] pickPut.check(): x3dTools is not defined"); flag = false; }
        if (typeof CASTLEBUILDER === "undefined") { console.log("[ ERRO ] pickPut.check(): castleBuilder is not defined"); flag = false; }
        if (flag) console.log("[ INFO ] pickPut.check(): OK");
        return flag;
    }
};
// ****************************************************************************
// Pick up and put down
// ****************************************************************************
var WORLD_ID = "ur5__WORLD";
var dummy_trafos = {};
function getWorldId() { return WORLD_ID; }
function putDown(id = "Load") {
    //console.log("[ INFO ] putDown() called @" + id);
    var ld = document.getElementById(id);
    var wd = document.getElementById(WORLD_ID);
    
    if (ld === null || wd === null) return 0;
    
    var traf = getTrafChain(id,WORLD_ID);
    //console.log("[ INFO ] putDown(): traf:");
    //console.log(traf);
    var resTraf = resultantTrafo(traf);
    //console.log("[ INFO ] putDown(): resTraf:");
    //console.log(resTraf);
    var t = createTransform(resTraf[0],resTraf[1]);
    
    t.appendChild(ld);
    wd.appendChild(t);

    while (dummy_trafos[id] !== undefined) {
        var trafo = document.getElementById(dummy_trafos[id]);
        trafo.parentNode.removeChild(trafo);
        delete dummy_trafos[id];
    }
    
    return 1;
}
function pickUp(TCP_ID, id = "Load") {
    //console.log("[ INFO ] pickUp() called @" + id);
    var ld = document.getElementById(id);
    var wd = document.getElementById(TCP_ID);
    
    //addClickEvent(id);
    
    if (ld === null || wd === null) return 0;
    
    var trafChTCP = resultantTrafo(getTrafChain(TCP_ID,WORLD_ID,true));
    var trafChLd = resultantTrafo(getTrafChain(id,WORLD_ID));
    
    var tran = [
        trafChLd[0][0] - trafChTCP[0][0],
        trafChLd[0][1] - trafChTCP[0][1],
        trafChLd[0][2] - trafChTCP[0][2]
    ];
    
    var d = new Date();
    var trafo_id = "dt_" + d.getTime();
    var t_inv = createTransform([0,0,0],invertAA(trafChTCP[1]),trafo_id);
    dummy_trafos[id] = trafo_id;
    
    //trafo_id = "dt_2_" + d.getTime();
    var t_for = createTransform(tran,trafChLd[1],trafo_id);
    //dummy_trafos.push(trafo_id);
    
    t_for.appendChild(ld);
    t_inv.appendChild(t_for);
    wd.appendChild(t_inv);
    
    return 1;
}
function getTrafChain(id, world, flag = undefined) {
    // TODO lehetne ezt slice-szal...
    var tran = [];
    var rota = [];
    var pt = document.getElementById(id);
    do {
        if(pt.hasAttribute("translation"))
            tran.push(pt.getAttribute("translation"));
        else
            tran.push("0 0 0");
        if(pt.hasAttribute("rotation"))
            rota.push(pt.getAttribute("rotation"));
        else
            rota.push("0 0 1 0");
        pt = pt.parentNode;
    } while (pt.id !== world);
    rota = rota.map(function (currentValue) {
        return currentValue.split(/[\s|\,]/).filter( (v) => v!=="" ).map( (v) => Number(v) );
    });
    tran = tran.map(function (currentValue) {
        return currentValue.split(/[\s|\,]/).filter( (v) => v!=="" ).map( (v) => Number(v) );
    });
    if (flag === undefined)
        return [tran.slice(1), rota.slice(1)];
    else
        return [tran, rota];
}
// ****************************************************************************
// Table w/ boxes
// ****************************************************************************
var boxes = [];
function registerBox(id) {
    boxes.push(id);
    return false;
}
function deleteBox(id) {
    if (boxes.indexOf(id) !== -1) {
        var newBoxes = [];
        boxes.forEach( (v, i) => { if (v !== id) newBoxes.push(v); else deleteCrds(i); } );
        boxes = newBoxes;
        
        var box = document.getElementById(id);
        box.parentNode.removeChild(box);
    }
    return false;
}
function getBoxes() {
    var boxes_copy = [];
    boxes.forEach( (v) => boxes_copy.push(v) );
    return boxes_copy;
}    
function addBox(tran, rot, size, color, world_id = "TABLETOP", transparency = '0') {
    var tr = document.createElement('Transform');
    tr.setAttribute("translation",tran.join(' '));
    tr.setAttribute("rotation",rot.join(' '));
    
    var d = new Date();
    var boxId = "Box_" + d.getTime();
    tr.setAttribute("id",boxId);
    registerBox(boxId);
    
    var tr2 = document.createElement('Transform');
    //tr2.setAttribute("translation", size.map( (v) => { return Number(v)/2 } ).join(' '));
    tr2.setAttribute("translation", "0 0 " + size[2]);
    tr2.setAttribute("rotation","0.707107 0.707107 0 3.141592");
    tr2.setAttribute("id",boxId + "@grab");
    
    var tr3 = document.createElement("Transform");
    tr3.setAttribute("translation", "0 0 " + size[2]/2);
    tr3.setAttribute("rotation", "0 0 1 0");
    
    var sh = document.createElement('Shape');
    var ap = document.createElement('Appearance');
    var ma = document.createElement('Material');
    ma.setAttribute("diffusecolor",color.join(' '));
    ma.setAttribute("transparency",transparency);
    ap.appendChild(ma);
    sh.appendChild(ap);
    var bx = document.createElement('Box');
    bx.setAttribute("size",size.join(' '));
    sh.appendChild(bx);
    tr3.appendChild(sh);
    tr2.appendChild(tr3);
    tr.appendChild(tr2);
    var wd = document.getElementById(world_id);
    wd.appendChild(tr);
    
    return 1;
}
const sizeX = 500;
const sizeZ = 500;
const boxSize = 50;
var crds = [];
var timeoutms = 2000;
function getCrds() {
    var crds_copy = [];
    crds.forEach( (v) => crds_copy.push(v) );
    return crds_copy;
}
function addCrds() {
    var flag = true;
    var rands;
    var d = new Date();
    var t = d.getTime() + timeoutms;
    var timeoutFlag = true;
    while (flag && timeoutFlag) {
        rands = getRands();
        flag = checkCollision(rands);
        d = new Date();
        timeoutFlag = d.getTime() < t;
    }
    if (!timeoutFlag) {
        return undefined;
    }
    crds.push(rands);
    return rands;
}
function getRands() {
    var rands = [];
    rands.push(Math.random()*(sizeX - 2 * boxSize));
    rands.push(Math.random()*(sizeZ - 2 * boxSize));
    return rands;
}
function deleteCrds(index) {
    var newCrds = [];
    crds.forEach( (v, i) => { if(i !== index) newCrds.push(v); } );
    crds = newCrds;
}
function addRandomBox() {
    rands = addCrds();
    var rotCrd = Math.random()*Math.PI/2;
    var crds_mod = crds.pop();
    crds_mod.push(rotCrd);
    crds.push(crds_mod);
    if (rands !== undefined){
        addBox([rands[0],rands[1],0],[0,0,1,rotCrd],[boxSize,boxSize,boxSize],[Math.random(),Math.random(),Math.random()]);
        return 1;
    } else return 0;
}
function checkCollision(rands) {
    var flag = false;
    if (crds !== [])
        crds.forEach( (v) => {
            var d = Math.pow(rands[0] - v[0], 2) + Math.pow(rands[1] - v[1], 2);
            if (d <= Math.pow(boxSize*2,2)) flag = flag | true;
        });
    return flag;
}
function createTable(translation,rotation = [0,0,1,0],tabletop_id = "TABLETOP") {
    var tr = document.createElement("Transform");
    tr.setAttribute("translation",[translation[0],translation[1],translation[2]-Math.min(sizeX,sizeZ)/20/2].join(' '));
    tr.setAttribute("rotation",rotation.join(' '));
    var sh = document.createElement("Shape");
    var ap = document.createElement("Appearance");
    var mt = document.createElement("Material");
    mt.setAttribute("diffuseColor",'0.2 0.1 0');
    mt.setAttribute("transparency",'0.25');
    var bx = document.createElement("Box");
    bx.setAttribute("size",[sizeX,sizeZ,Math.min(sizeX,sizeZ)/20].join(' '));
    ap.appendChild(mt);
    sh.appendChild(ap);
    sh.appendChild(bx);
    tr.appendChild(sh);
    
    var tr2 = document.createElement("Transform");
    tr2.setAttribute('id',tabletop_id);
    tr2.setAttribute('translation',[-sizeX/2+boxSize,-sizeZ/2+boxSize,Math.min(sizeX,sizeZ)/20/2].join(' '));
    tr.appendChild(tr2);
    
    return tr;
}
function generateBoxes() {
    while(addRandomBox());
    return boxes.length;
}
// ****************************************************************************
// Maths
// ****************************************************************************
function resultantTrafo(data) {
    var tran = [];
    var rot = [];
    
    data[0].forEach( (v) => tran.unshift(v) );
    data[1].forEach( (v) => rot.unshift(v) );
    
    var crds = [0,0,0];
    var basx = [1,0,0];
    var basy = [0,1,0];
    var basz = [0,0,1];
    var rotv = [0,0,1];
    var norm = 0;
    
    for (var i = 0; i < tran.length; ++i) {
        crds = translate(crds,tran[i],basx,basy,basz);
        
        if (rot[i][0] === 0 && rot[i][1] === 0 && rot[i][2] === 0 && rot[i][3] === 0)
            rot[i][2] = 1;
        
        rotv = rebase(rot[i].slice(0,-1),basx,basy,basz);
        
        basx = rotate(basx,rotv,rot[i].slice(-1));
        basy = rotate(basy,rotv,rot[i].slice(-1));
        basz = rotate(basz,rotv,rot[i].slice(-1));
    }
    rotv = getAxisAngle([basx,basy,basz]);
    return [crds, rotv.map( (v,i) => { if(i < 3) return -v; else return v; } )];
}
function rebase(r,bx,by,bz) {
    var v = [0, 0, 0];
    for (var i = 0; i < 3; ++i)
        v[i] = r[0] * bx[i] + r[1] * by[i] + r[2] * bz[i];
        
    var norm = Math.sqrt(Math.pow(v[0],2) + Math.pow(v[1],2) + Math.pow(v[2],2));
    v = v.map( (cv) => cv / norm );
    
    return v;
}
function translate(r,w,bx,by,bz) {
    for (var i = 0; i < 3; ++i)
        r[i] += w[0] * bx[i] + w[1] * by[i] + w[2] * bz[i];
    return r;
}
function rotate(r,w,a) {
    var r1 = r[0];
    var r2 = r[1];
    var r3 = r[2];
    
    var w1 = w[0];
    var w2 = w[1];
    var w3 = w[2];
    
    var rv1 = r1*w1*w1 + r2*w1*w2 + r3*w1*w3 + r1*Math.cos(a) - r1*w1*w1*Math.cos(a) - r2*w1*w2*Math.cos(a) - r3*w1*w3*Math.cos(a) + r3*w2*Math.sin(a) - r2*w3*Math.sin(a);
    var rv2 = r1*w1*w2 + r2*w2*w2 + r3*w2*w3 + r2*Math.cos(a) - r1*w1*w2*Math.cos(a) - r2*w2*w2*Math.cos(a) - r3*w2*w3*Math.cos(a) - r3*w1*Math.sin(a) + r1*w3*Math.sin(a);
    var rv3 = r1*w1*w3 + r2*w2*w3 + r3*w3*w3 + r3*Math.cos(a) - r1*w1*w3*Math.cos(a) - r2*w2*w3*Math.cos(a) - r3*w3*w3*Math.cos(a) + r2*w1*Math.sin(a) - r1*w2*Math.sin(a);
    
    return [rv1, rv2, rv3];
}
function invertAA(AA) {
    return AA.map( (v,i) => { if(i < 3) return -v; else return v; });
}
function getAxisAngle(m) {
    // TODO source
    var angle,x,y,z; // variables for result
	var epsilon = 0.0001; // margin to allow for rounding errors
	var epsilon2 = 0.0001; // margin to distinguish between 0 and 180 degrees
	// optional check that input is pure rotation, 'isRotationMatrix' is defined at:
	// https://www.euclideanspace.com/maths/algebra/matrix/orthogonal/rotation/
	//assert isRotationMatrix(m) : "not valid rotation matrix" ;// for debugging
	if ((Math.abs(m[0][1]-m[1][0])< epsilon) && (Math.abs(m[0][2]-m[2][0])< epsilon) && (Math.abs(m[1][2]-m[2][1])< epsilon)) {
		// singularity found
		// first check for identity matrix which must have +1 for all terms
		//  in leading diagonaland zero in other terms
		if ((Math.abs(m[0][1]+m[1][0]) < epsilon2) && (Math.abs(m[0][2]+m[2][0]) < epsilon2) && (Math.abs(m[1][2]+m[2][1]) < epsilon2) && (Math.abs(m[0][0]+m[1][1]+m[2][2]-3) < epsilon2)) {
			// this singularity is identity matrix so angle = 0
			return [0,0,1,0]; // zero angle, arbitrary axis
		}
		// otherwise this singularity is angle = 180
		angle = Math.PI;
		var xx = (m[0][0]+1)/2;
		var yy = (m[1][1]+1)/2;
		var zz = (m[2][2]+1)/2;
		var xy = (m[0][1]+m[1][0])/4;
		var xz = (m[0][2]+m[2][0])/4;
		var yz = (m[1][2]+m[2][1])/4;
		if ((xx > yy) && (xx > zz)) { // m[0][0] is the largest diagonal term
			if (xx< epsilon) {
				x = 0;
				y = 0.7071;
				z = 0.7071;
			} else {
				x = Math.sqrt(xx);
				y = xy/x;
				z = xz/x;
			}
		} else if (yy > zz) { // m[1][1] is the largest diagonal term
			if (yy< epsilon) {
				x = 0.7071;
				y = 0;
				z = 0.7071;
			} else {
				y = Math.sqrt(yy);
				x = xy/y;
				z = yz/y;
			}	
		} else { // m[2][2] is the largest diagonal term so base result on this
			if (zz< epsilon) {
				x = 0.7071;
				y = 0.7071;
				z = 0;
			} else {
				z = Math.sqrt(zz);
				x = xz/z;
				y = yz/z;
			}
		}
		return [x,y,z,angle]; // return 180 deg rotation
	}
	// as we have reached here there are no singularities so we can handle normally
	var s = Math.sqrt((m[2][1] - m[1][2])*(m[2][1] - m[1][2]) + (m[0][2] - m[2][0])*(m[0][2] - m[2][0]) + (m[1][0] - m[0][1])*(m[1][0] - m[0][1])); // used to normalise
	if (Math.abs(s) < 0.0001) s = 1; 
		// prevent divide by zero, should not happen if matrix is orthogonal and should be
		// caught by singularity test above, but I've left it in just in case
	angle = Math.acos(( m[0][0] + m[1][1] + m[2][2] - 1)/2);
	x = (m[2][1] - m[1][2])/s;
	y = (m[0][2] - m[2][0])/s;
	z = (m[1][0] - m[0][1])/s;
    return [x,y,z,angle];
}
function AAToMatrix(rx,ry,rz) {
    // by HM
    
    var angle = Math.sqrt(Math.pow(rx,2) + Math.pow(ry,2) + Math.pow(rz,2));
    var x = rx/angle;
    var y = ry/angle;
    var z = rz/angle;
    
    //for easier typing
    var c = Math.cos(angle);
    var s = Math.sin(angle);
    var t = 1 - c;
    
    //matrix elements
    var m00 = t * x * x + c;
    var m01 = t * x * y - z * s;
    var m02 = t * x * z + y * s;
    var m10 = t * x * y + z * s;
    var m11 = t * y * y + c;
    var m12 = t * y * z - x * s;
    var m20 = t * x * z - y * s;
    var m21 = t * y * z + x * s;
    var m22 = t * z * z + c;
    
    var M = [ [m00,m01,m02], [m10,m11,m12], [m20,m21,m22] ];
    
    return M;
}