// ***************************************************************************
// Interface
// ***************************************************************************
const INTERPOLATE = {
    check: function () {
        var flag = true;
        if (typeof LINKAGEDESIGNER === "undefined") { console.log("[ ERRO ] interpolate.check(): linkageDesigner is not defined"); flag = false; }
        if (typeof PUBSUBFRAMEWORK === "undefined") { console.log("[ ERRO ] interpolate.check(): pubsubFramework is not defined"); flag = false; }
        if (typeof PICKPUT === "undefined") { console.log("[ ERRO ] interpolate.check(): pickPut is not defined"); flag = false; }
        if (flag) console.log("[ INFO ] interpolate.check(): OK");
        return flag;
    }
};
// ***************************************************************************
// Content
// ***************************************************************************
var lastCommand;
var INT_TIMER;
var stopTime = 0;
var ip_data = {
    joints: [],
    startValues: [],
    endValues: [],
    fps_target: 60,
    time_t0: 0,
    time_t1: 0,
    m: [],
    step: 0,
    ticks: 0,
    abortFlag: false,
    responseFlag: true
};
function setUpInterpolation(data,flag = true) {
    ip_data.abortFlag = false;
    ip_data.responseFlag = flag;
    ip_data.joints = data[0];
    ip_data.endValues = data[1];
    
    var d = new Date();
    ip_data.time_t0 = d.getTime();
    ip_data.time_t1 = ip_data.time_t0 + data[2];
    
    ip_data.startValues = [];
    ip_data.joints.forEach(function(currentValue) {
        ip_data.startValues.push(getJointValue(currentValue)[0]);
    });
    
    ip_data.m = [];
    ip_data.endValues.forEach(function (currentValue, index){
        ip_data.m.push((currentValue - ip_data.startValues[index])/data[2]);
    });
    if(flag) {
        lastCommand.status = 1;
        publishMessage("Response@NRC",lastCommand);
        lastCommand.status = 2;
    } else lastCommand = { "command": { "param": data } };
    ip_data.step = Math.floor(1000/ip_data.fps_target);
    ip_data.ticks = 0;
    interpolating();
}
function interpolating(){
    if(!ip_data.abortFlag) {
        var d = new Date();
        var time_t = d.getTime();
        ++ip_data.ticks;
        if (time_t >= ip_data.time_t1) {
            //End interpolation
            ip_data.joints.forEach(function (currentValue, index){
                /*if(currentValue === "gripR1")
                    SetGripper(currentValue, ip_data.endValues[index], ["gripR3","gripR5"]);
                else*/
                    SetVar_BB(currentValue, ip_data.endValues[index]);
            });
            lastCommand.command.param[2] = [lastCommand.command.param[2], time_t - ip_data.time_t0, ip_data.ticks / (time_t - ip_data.time_t0) * 1000];
            if(ip_data.responseFlag) publishMessage("Response@NRC",lastCommand);
        } else {
            //Interpolating...
            ip_data.joints.forEach(function (currentValue, index){
                var jointValue_t = ip_data.m[index] * (time_t - ip_data.time_t0) + ip_data.startValues[index];
                /*if(currentValue === "gripR1")
                    SetGripper(currentValue, jointValue_t, ["gripR3","gripR5"]);
                else*/
                    SetVar_BB(currentValue, jointValue_t);
            });
            INT_TIMER = setTimeout(interpolating, ip_data.step);
        }
    }
}
function pauseInterpolating() {
    clearTimeout(INT_TIMER);
    var d = new Date();
    stopTime = d.getTime();
}
function stopInterpolating() {
    clearTimeout(INT_TIMER);
    ip_data.abortFlag = true;
}
function startInterpolating() {
    if (stopTime !== 0 && !ip_data.abortFlag) {
        var d = new Date();
        var dt = d.getTime() - stopTime;
        ip_data.time_t1 += dt;
        ip_data.time_t0 += dt;
        stopTime = 0;
        interpolating();
        return 0;
    } else {
        return -1;
    }
}
function getCurrentPosition() {
    var ret = {};
    for(var id in id2joint)
        ret[id] = getJointValue(id);
    return ret;
}
function getInverse(targetPosition) {
    var currentPosition = getCurrentPosition();
    currentPosition = ["q1","q2","q3","q4","q5","q6"].map( (v) => currentPosition[v][0]*Math.PI/180 );
    //console.log(targetPosition);
    if(targetPosition.length === 7)
        targetPosition = targetPosition.slice(0,3).concat(targetPosition.slice(3,-1).map( (v) => v*targetPosition[6] ));
    //console.log(targetPosition);
    var rotMatrix = AAToMatrix(targetPosition[3],targetPosition[4],targetPosition[5]);
    
    publishMessage("Request4Inverse",{
        "session": listenSession,
        "targetPosition":
            [
                [rotMatrix[0][0],rotMatrix[0][1],rotMatrix[0][2],targetPosition[0]],
                [rotMatrix[1][0],rotMatrix[1][1],rotMatrix[1][2],targetPosition[1]],
                [rotMatrix[2][0],rotMatrix[2][1],rotMatrix[2][2],targetPosition[2]],
                [0,0,0,1]],
        "currentPosition": currentPosition
    });
}