// ***************************************************************************
// Interface
// ***************************************************************************
const PUBSUBFRAMEWORK = {
    check: function () {
        var flag = true;
        if (flag) console.log("[ INFO ] pubsubFramework.check(): OK");
        return flag;
    }
};
// ***************************************************************************
// Content
// ***************************************************************************
// Def topics
var WELCOME_TOPIC = "Welcome";
var RE_WELCOME_TOPIC = "Re: Welcome";
var RE_RE_WELCOME_TOPIC = "Re: Re: Welcome";
var SUBSCRIBE_TOPIC = "Subscribe";
var RE_SUBSCRIBE_TOPIC = "Re: Subscribe";
var UNSUBSCRIBE_TOPIC = "Unsubscribe";
var RE_UNSUBSCRIBE_TOPIC = "Re: Unsubscribe";

// Sockets
var listenAddress;
var publishAddress;
var listenSocket;
var publishSocket;

// The Node-RED ID of the listenSocket connection
var listenSession;

function listenSocketConnect() {
    listenSocket = new WebSocket(listenAddress);
    listenSocket.onmessage = function (event) {
        messageHandler(event);
    };
    listenSocket.onopen = function(event){
        console.log("[ INFO ] listenSocket open @" + listenAddress);
    };
    listenSocket.onclose = function(e) {
        console.log('Connection lost @listenSocket. Reconnect will be attempted in 1 second', e.reason);
        if(publishSocket.readyState === WebSocket.OPEN) publishSocket.close();
        setTimeout(function() {
            listenSocketConnect();
        }, 1000);
    };
    listenSocket.onerror = function(err) {
        console.error('Error encountered @listenSocket: ', err.message, 'Closing socket');
        listenSocket.close();
        if(publishSocket.readyState === WebSocket.OPEN) publishSocket.close();
    };
}
function publishSocketConnect(listenSessionId) {
    publishSocket = new WebSocket(publishAddress);
    publishSocket.onopen = function(event){ 
        console.log("[ INFO ] publishSocket open @" + publishAddress);
        publishMessage("Re: Welcome", listenSessionId);
    };
    publishSocket.onclose = function(e) {
        console.log('Connection lost @publishSocket', e.reason);
        if(listenSocket.readyState === WebSocket.OPEN) listenSocket.close();
    };
    publishSocket.onerror = function(err) {
        console.error('Error encountered @publishSocket: ', err.message, 'Closing socket');
        publishSocket.close();
        if(listenSocket.readyState === WebSocket.OPEN) listenSocket.close();
    };
}
function publishMessage(topic,payload) { publishSocket.send(JSON.stringify({ "topic": topic, "payload": payload })); }
function subscribe(topics) { publishMessage(SUBSCRIBE_TOPIC, topics); }
function unsubscribe(topics) { publishMessage(UNSUBSCRIBE_TOPIC, topics); }
function messageHandler(event){
    var data = JSON.parse(event.data);
    switch(data.topic){
        case WELCOME_TOPIC:
            //console.log("Opening publishSocket...");
            listenSession = data.payload;
            publishSocketConnect(data.payload);
            break;
        case RE_RE_WELCOME_TOPIC:
            onConnectionAlive();
            break;
        case RE_SUBSCRIBE_TOPIC:
            console.log("[ INFO ] server response: " + data.payload + " (" + data.topic + ")");
            break;
        case RE_UNSUBSCRIBE_TOPIC:
            console.log("[ INFO ] server response: " + data.payload + " (" + data.topic + ")");
            break;
        default:
            handlingEE(data);
    }
}
function handlingEE(data) {
    console.log("[ WARN ] Unhandled topic: '" + data.topic + "'");
}
function onConnectionAlive() {
    console.log("[ INFO ] Connection is live");
}