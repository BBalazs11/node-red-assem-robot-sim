// ***************************************************************************
// Interface
// ***************************************************************************
const CASTLEBUILDER = {
    check: function () {
        var flag = true;
        if (typeof X3DTOOLS === "undefined") { console.log("[ ERRO ] castleBuilder.check(): x3dTools is not defined"); flag = false; }
        if (typeof PICKPUT === "undefined") { console.log("[ ERRO ] chat.check(): pickPut is not defined"); flag = false; }
        if (flag) console.log("[ INFO ] chat.check(): OK");
        return flag;
    }
};
// ***************************************************************************
// Content
// ***************************************************************************
var STATE = 0;
var POINTER_BOX;
var BUILDER_BOXES = [];
var POINTER_TRANSFORM;
var SELECTED_BOX = {};

function deleteBuilderBoxes() {
    BUILDER_BOXES.forEach( (v) => { deleteBox(v[0]); getElem(v[1]).setAttribute('render','true'); } );
    BUILDER_BOXES = [];
}
function setTableListener(table_id, world_id = "world", transform_id = "Eufrozina") {
    document.getElementById(table_id).parentNode.children[0].addEventListener('mousemove', function(event){
        if(event.normalZ >= 0.90 && event.normalX <= 0.30 && event.normalY <= 0.30 && STATE === 0) {
            var tr;
            if(POINTER_BOX === undefined) {
                //console.log(event);
                var wd = document.getElementById(world_id);
                tr = document.createElement('Transform');
                tr.setAttribute('translation',[event.worldX,event.worldY,event.worldZ].join(' '));
                tr.setAttribute('rotation',[0,0,1,0].join(" "));
                tr.setAttribute('id', transform_id);
                wd.appendChild(tr);
                addBox([0,0,0],[0,0,1,0],[boxSize,boxSize,boxSize],[0.41,0.41,0.41],transform_id,'0.8');
                
                POINTER_BOX = getBoxes()[getBoxes().length - 1];
                POINTER_TRANSFORM = transform_id;
                
                tr.children[0].addEventListener('click', function(event){
                    //console.log(event);
                    if(STATE === 0 && event.button === 1) {
                        STATE = 1;
                        var efi = document.getElementById(transform_id);
                        efi.setAttribute('render', false);
                        
                        if (SELECTED_BOX.hasOwnProperty('color'))
                            addBox(
                                efi.getAttribute('translation').split(" ").map( (v) => Number(v) ),
                                efi.getAttribute('rotation').split(" ").map( (v) => Number(v) ),
                                [boxSize,boxSize,boxSize],
                                SELECTED_BOX.color,
                                world_id);
                        else
                            addBox(
                                efi.getAttribute('translation').split(" ").map( (v) => Number(v) ),
                                efi.getAttribute('rotation').split(" ").map( (v) => Number(v) ),
                                [boxSize,boxSize,boxSize],
                                [0,1,1],
                                world_id);
                        
                        var lastboxid = getBoxes()[getBoxes().length - 1];
                        BUILDER_BOXES.push([lastboxid, SELECTED_BOX.id]);
                        var bxsh = document.getElementById(lastboxid).children[0].children[0].children[0];
                        
                        bxsh.addEventListener('mousemove', function(event) {
                            if(event.normalZ >= 0.99 && event.normalX <= 0.01 && event.normalY <= 0.01 && STATE === 0) {
                                var tr = document.getElementById(transform_id);
                                tr.setAttribute('translation',[event.worldX,event.worldY,event.worldZ].join(' '));
                                tr.setAttribute('rotation',[0,0,1,0].join(" "));
                            }
                        }, false);
                    }
                }, false);
            } else {
                //console.log(event);
                tr = document.getElementById(transform_id);
                tr.setAttribute('render', true);
                tr.setAttribute('translation',[event.worldX,event.worldY,event.worldZ].join(' '));
                tr.setAttribute('rotation',[0,0,1,0].join(" "));
            }
        }
    }, false);
}
function addClickEvent(box_id) {
    getElem(box_id).children[0].children[0].children[0].addEventListener('click', function(event) {
        chatPost(sysMsg("Selected " + box_id));
        
        if(event.button === 2) {
            document.getElementById(box_id).setAttribute('render','false'); 
            SELECTED_BOX["id"] = box_id;
            SELECTED_BOX["color"] = getElem(box_id).children[0].children[0].children[0].children[0].children[0].getAttribute('diffuseColor').split(" ").map( (v) => Number(v) );
            STATE = 0;
            var efi = document.getElementById(POINTER_TRANSFORM);
            efi.setAttribute('render', true);
        }
    }, false);
}