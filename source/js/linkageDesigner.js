// ***************************************************************************
// Interface
// ***************************************************************************
const LINKAGEDESIGNER = {
    check: function () {
        var flag = true;
		if (flag) console.log("[ INFO ] linkageDesigner.check(): OK");
		return flag;
    }
};
// ***************************************************************************
// Content
// ***************************************************************************
var allJointMap={};
var id2joint={};
var jointLimit={};

class Joint{
	constructor(type,varName){
		this.type = type;
		this.varName=varName;
		this.varNamels=varName.split("_");
		this.varNamemap={};
		var No=1;
		if(type != "Fixed"){
			No=this.varNamels.length;
		}
		// varNamels defines the joint variables
		this.min=[];
		this.max=[];
		this.val=[];
		this.dstep=[];
		for(var i=0; i<No;i++){
		    var jvarName=this.varNamels[i];
			this.varNamemap[jvarName]=i;
		    if(jointLimit.hasOwnProperty(jvarName)){
				this.min.push(jointLimit[jvarName][0]);
				this.max.push(jointLimit[jvarName][1]);
				this.val.push(0);
				this.dstep.push(jointLimit[jvarName][2]);
			}
			else{
				this.min.push(-100);
				this.max.push(100);
				this.val.push(0);
				this.dstep.push(1);
			}
		}
		//this.jointSliders=[];
		this.x3Node = null;
		if(type != "Fixed"){
			//this.getSliderID = function(i) {return 'slider_'+this.varNamels[i];}; // Slider stuff
			this.getValueID = function(i) {return 'value'+this.varNamels[i];};
			this.SetJValue = function(name,value) {
				var i = this.varNamemap[name];
				if (typeof i !== "number") return NaN; 
				this.val[i]=value;
				return this.val;
				};
				
			}
		}
}

function SetVar(values, handle, unencoded, tap, positions ) {
		var str=this.target.id;
		var strls=str.split("_");
		var jointVar = strls[1];
		var jointItem=allJointMap[id2joint[strls[1]]];
		jointItem.SetJValue(jointVar,values[1]);         //set the joint value to jointVar joint record
		//console.log(jointVar+ "->" +values[1]/*+" : "+jointItem.getTrafoType(jointVar)*/);
		//Update the x3D model with the joint values
		setJointValue(jointVar);
	}
	
function SetVar_BB(id, value) {
		var jointVar = id;
		var jointItem=allJointMap[id2joint[id]];
		jointItem.SetJValue(jointVar,value);         //set the joint value to jointVar joint record
		//console.log(jointVar+ "->" +value/*+" : "+jointItem.getTrafoType(jointVar)*/);
		//Update the x3D model with the joint values
		setJointValue(jointVar);
	}
	
function setJointValue(jVar){
		var jointItem = allJointMap[id2joint[jVar]];
		var x3Node = jointItem.x3Node;
		var jointValues = jointItem.val;
		switch(jointItem.type) {
    		case "Rotational":
				var valueString = x3Node.getAttribute("rotation").trim();
				//TODO azért az, hogy ez " "-vel van splittelve egy elég előzékeny dolog
				var splitted = valueString.split(" ");
				splitted[3]=jointItem.val[0] * Math.PI / 180;
				x3Node.setAttribute("rotation", splitted.join(" "));
				break;
			case "Translational":	
				var valueString = x3Node.getAttribute("translation").trim();
				var splitted = valueString.split(" ");
				splitted[2]=String(jointItem.val[0]);
				x3Node.setAttribute("translation", splitted.join(" "));
				break;		
			case "Planar":
				//Set Rotation
				var valueString = x3Node.getAttribute("rotation").trim();
				var splitted = valueString.split(" ");
				splitted[3]=jointItem.val[2] * Math.PI / 180;
				x3Node.setAttribute("rotation", splitted.join(" "));
				//Set Translation in X,Y direction
				valueString = x3Node.getAttribute("translation").trim();
				splitted = valueString.split(" ");
				splitted[0]=String(jointItem.val[0]);
				splitted[1]=String(jointItem.val[1]);
				x3Node.setAttribute("translation", splitted.join(" "));
				break;
			case "Universal":
				var ang1 = jointItem.val[0] * Math.PI / 180;
				var w1= Math.cos(ang1/2);
				var x1= 0;
				var y1= 0;
				var z1= Math.sin(ang1/2);
				
				var ang2 = jointItem.val[1] * Math.PI / 180;
				var w2 = Math.cos(ang2/2);
				var x2 = Math.sin(ang2/2);
				var y2 = 0;
				var z2 = 0; 
				
				var w = (w1*w2 - x1*x2 - y1*y2 - z1*z2);
				var x = (w1*x2 + x1*w2 + y1*z2 - z1*y2);
				var y = (w1*y2 - x1*z2 + y1*w2 + z1*x2);
				var z = (w1*z2 + x1*y2 - y1*x2 + z1*w2);
				
				var size = Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2)+Math.pow(z, 2));
				var splitted = [x/size,y/size,z/size,2*Math.acos(w)];
				x3Node.setAttribute("rotation", splitted.join(" "));
				break;
			
		}
	}

function getJointValue(jvarName){
		var jointItem=allJointMap[id2joint[jvarName]];
		if (typeof jointItem === "undefined") return null;
		var x3Node = jointItem.x3Node;
		var ret=[0,0,0];
		switch(jointItem.type) {
    		case "Rotational":
				var valueString = x3Node.getAttribute("rotation").trim();
				var splitted = valueString.split(" ");
				ret[0] = splitted[3] * 180 / Math.PI;
				return ret;
				//break;
			case "Translational":	
				var valueString = x3Node.getAttribute("translation").trim();
				var splitted = valueString.split(" ");
				ret[0] = splitted[2];
				return ret;
				//break;
			case "Planar":	
				var valueString = x3Node.getAttribute("translation").trim();
				var splitted = valueString.split(" ");
				ret[0]=splitted[0];
				ret[1]=splitted[1];
				valueString = x3Node.getAttribute("rotation").trim();
				splitted = valueString.split(" ");
				ret[2]=splitted[3]* 180 / Math.PI;
				return ret;
				//break;
			case "Universal":	
				var valueString = x3Node.getAttribute("rotation").trim();
				var splitted = valueString.split(" ");
				ret[0]=2*Math.atan2(splitted[3],splitted[0]);
				ret[1]=2*Math.atan2(splitted[1],splitted[0]);
				return ret;
				//break;
			
				}
	}	
	
/* // Slider stuff
function BuildSlider(jnode){
    console.log("BuildSlider is called");
	for( var j=0; j< jnode.varNamels.length; j++){
	var id=jnode.getSliderID(j);
	var slider = document.getElementById(id);
	var itemSlider=noUiSlider.create(slider, {
			start: [jnode.min[j],jnode.val[j], jnode.max[j]],
			range: {
				'min': [ jnode.min[j] ],
				'max': [ jnode.max[j] ]
			},
			pips: { // Show a scale with the slider
				mode: 'range',
				density: 3
			},
			step: jnode.dstep[j],
			margin: 0,
			connect: [false,true, true,false],
			tooltips: [false,true,false]
			});
	// bind  the slide event to setDrivingVariables function .		
	slider.noUiSlider.on('slide', SetVar);		
	jnode.jointSliders.push(itemSlider);
	}
		
};*/


function InitLinkage(){
	console.log("InitLinkage function called");
	var treeWalker = document.createTreeWalker(
        document.body,
        NodeFilter.SHOW_ELEMENT,
        {
            acceptNode: function(node) {
                if (  /^.*JOINT\(.*\)\(.*\)/.test(node.id) ) {
					return NodeFilter.FILTER_ACCEPT;
                }
            }
        },
        false
	);

	while(treeWalker.nextNode()) {
		var joint = treeWalker.currentNode.id.match(/^.*JOINT\((.*)\)\((.*)\)/);
		var jType = joint[1];
		var jVar = joint[2];
		var item = new Joint(jType, jVar);
		item.x3Node = treeWalker.currentNode;
		allJointMap[jVar]=item;
		if(jType != "Fixed"){
			for( var j=0; j< item.varNamels.length; j++){
			    id2joint[item.varNamels[j]]=item.varName;
				//str="<div class='floating-box'><div id='"+item.getSliderID(j)+"'  class='slider'></div>"; // Slider Stuff
				//str=str+"<div id='"+item.getValueID(j)+"'  class='SliderVal'>"+item.varNamels[j]+"</div></div>"; // Slider stuff
				//$('body').append(str); // Slider stuff
				//item.val[j]=getJointValue(item.varNamels[j])[j]; // Slider stuff
			 }
			     
			 //BuildSlider(item); // Slider stuff
		}
	}
}

function AddJointLimit(varName,min,max,dstep){
    jointLimit[varName] = [min, max, dstep];
}