// ***************************************************************************
// Interface
// ***************************************************************************
const CHAT = {
    check: function () {
        var flag = true;
        if (typeof PICKPUT === "undefined") { console.log("[ ERRO ] chat.check(): pickPut is not defined"); flag = false; }
        if (typeof PUBSUBFRAMEWORK === "undefined") { console.log("[ ERRO ] chat.check(): pubsubFramework is not defined"); flag = false; }
        if (typeof CASTLEBUILDER === "undefined") { console.log("[ ERRO ] chat.check(): castleBuilder is not defined"); flag = false; }
        if (typeof TASKBUILDER === "undefined") { console.log("[ ERRO ] chat.check(): taskBuilder is not defined"); flag = false; }
        if (typeof INTERPOLATE === "undefined") { console.log("[ ERRO ] chat.check(): interpolate is not defined"); flag = false; }
        if (typeof X3DTOOLS === "undefined") { console.log("[ ERRO ] chat.check(): x3dTools is not defined"); flag = false; }
        if (flag) console.log("[ INFO ] chat.check(): OK");
        return flag;
    }
};
// ***************************************************************************
// Chatkezelés
// ***************************************************************************
// Enterfigyelés
function chatKeyCheck (event) {
    if (event.keyCode == 13) {
        sendmessage();
        return false;
    }
}
// Üzenetküldés
function sendmessage() {
    var Msg = document.getElementById('Message');
    var msg =  Msg.value;
    Msg.value = "";
    chatPost(userMsg(msg));
    
    var trafos;
    
    if (msg === "boxhiking") {
        chatPost(sysMsg("Currently under development"));
        /*var boxes = getBoxes();
        trafos = boxes.map( (v) => {
            return resultantTrafo(getTrafChain(v+"@grab","ur5__WORLD",true));
        });
        publishMessage(undefined,{
            "status":0,
            "target":21,
            "id": (new Date()).getTime(),
            "command":{
                "cmd": "4Mathematica",
                "param": [trafos, boxes]
            },
            "desc":"Whatever"
        });*/
    } else if (msg === "?") {
        chatPost(sysMsg("Type 'boxhiking' to load a graph to iterate through the boxes in the scene and raise them up a bit"));
        chatPost(sysMsg("Type 'buildMode' to initialize building mode"));
        chatPost(sysMsg("Type 'build@builder' to start processing the defined sequence"));
        chatPost(sysMsg("Type 'reset@builder' to reset the building environment"));
    } else if (msg === "reset@builder") {
        chatPost(sysMsg("Resetting the builder..."));
        getBoxes().forEach(function(v,i) {
            var ld = document.getElementById(v);
            var wd = document.getElementById("TABLETOP");
            if (ld === null || wd === null) return 0;
            
            var CRDS = getCrds();
            var crd = CRDS[i];
            ld.setAttribute("translation",[crd[0],crd[1],0].join(' '));
            ld.setAttribute("rotation",[0,0,1,crd[2]].join(' '));

            wd.appendChild(ld);
            ld.setAttribute('render','true');
            addClickEvent(v);

            return 1;
        });
        deleteBuilderBoxes();
    } else if (msg === "build@builder") {
        if(BUILDER_BOXES.length === 0) {
            chatPost(sysMsg("No sequence has been defined"));
        } else {
            TRAFOS = BUILDER_BOXES.map( (v) => {
                return [
                    resultantTrafo(getTrafChain(v[0]+"@grab","ur5__WORLD",true)),
                    resultantTrafo(getTrafChain(v[1]+"@grab","ur5__WORLD",true))
                ];
            });
            BOX_IDs = BUILDER_BOXES.map( (v) => {
                return v[1];
            });   
            deleteBuilderBoxes();
            chatPost(sysMsg("Starting the construction..."));
            var mtime = 1000;
            var entries = [];
            var subEntries;
            BOX_IDs.forEach( (v,i) => {
                subEntries = [];
                subEntries.push(SafetyDistance(TRAFOS[i][1][0].concat(TRAFOS[i][1][1]),mtime));
                subEntries.push(OpenGripper(500));
                subEntries.push(CloseIn(TRAFOS[i][1][0].concat(TRAFOS[i][1][1]),mtime));
                subEntries.push(CloseGripper(500));
                subEntries.push(PickTask(v));
                subEntries.push(SafetyDistance(TRAFOS[i][1][0].concat(TRAFOS[i][1][1]),mtime));
                subEntries.push(subEntry(listenSession,"Interpolate",[["q1","q2","q3","q4","q5","q6"],[135,-90,-90,-90,90,-180],mtime]));
                
                subEntries.push(SafetyDistance(TRAFOS[i][0][0].concat(TRAFOS[i][0][1]),mtime));
                subEntries.push(CloseIn(TRAFOS[i][0][0].concat(TRAFOS[i][0][1]),mtime));
                subEntries.push(PutTask(v));
                subEntries.push(OpenGripper(500));
                subEntries.push(SafetyDistance(TRAFOS[i][0][0].concat(TRAFOS[i][0][1]),mtime));
                subEntries.push(subEntry(listenSession,"Interpolate",[["q1","q2","q3","q4","q5","q6"],[45,-90,-90,-90,90,-180],mtime]));
                
                entries.push(complexEntry(listenSession + "_" + i, i===0 ? [0] : [listenSession + "_" + (i-1)], subEntries));
            });
            var graph = taskGraph("BuildedTask@" + listenSession, entries);
            document.getElementById("createdGraphList").appendChild(createGraphListElement(graph));
            publishMessage("Graph@NRC",graph);
        }
    } else if(msg === "buildMode") {
        subscribe(["Task@" + listenSession, "Inverse@" + listenSession]);
        setUpInterpolation([["q1","q2","q3","q4","q5","q6"],[45,-90,-90,-90,90,-180],1000],false);
    } else {
        chatPost(sysMsg("I could not do such a thing!"));
    }
}
function chatPost(msg) {
    var chat = document.getElementById("Chat");
    chat.appendChild(msg);
    chat.scrollTop = chat.scrollHeight;
}
function sysMsg(msg) {
    var entryholder = document.createElement("div");
    var att = document.createAttribute("class");
    att.value = "w3-panel"; 
    entryholder.setAttributeNode(att);
    
    var entry = document.createElement("div");
    att = document.createAttribute("class");
    att.value = "w3-container w3-round-large w3-light-gray w3-padding w3-left"; 
    entry.setAttributeNode(att);
    entry.appendChild(document.createTextNode(msg));
    
    entryholder.appendChild(entry);
    return entryholder;
}
function userMsg(msg) {
    var entryholder = document.createElement("div");
    var att = document.createAttribute("class");
    att.value = "w3-panel"; 
    entryholder.setAttributeNode(att);
    
    var entry = document.createElement("div");
    att = document.createAttribute("class");
    att.value = "w3-container w3-round-large w3-blue w3-padding w3-right"; 
    entry.setAttributeNode(att);
    entry.appendChild(document.createTextNode(msg));
    
    entryholder.appendChild(entry);
    return entryholder;
}